FROM ubuntu:18.04
RUN apt-get update -qq -y ;\
    apt-get -qq -y install wget gnupg
RUN echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_18.04/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list; \
    wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/xUbuntu_18.04/Release.key -O- | apt-key add - ;\
    apt-get update -qq -y ;\
    apt-get -qq -y install podman
