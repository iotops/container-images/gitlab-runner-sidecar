# Build the gitlab-runner-sidecar image.
#
# Usage:
#   make (clean|build|push|run)

all: clean build push run

clean:
	# cleaning temporary files
	find . -type f -iname \*~ -delete
build:
	# building image
	podman build -t gitlab-runner-sidecar .
push: build
	# pushing image
run:
	# running image
	podman run --rm -it gitlab-runner-sidecar bash
